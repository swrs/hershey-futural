# Hershey-Futural

The *Hershey Futural* is a typeface, one of the many typefaces designed by Dr. Allen Vincent Hershey in the late 60s. It is an adoption from [Luuse's Hershey-Noailles fonts set](https://gitlab.com/Luuse/Villa-Noailles/font-hershey-noailles), itself forked from [hersheytextjs](https://github.com/techninja/hersheytextjs), ported from the [Hershey Text Inkscape Plugin](https://www.evilmadscientist.com/2011/hershey-text-an-inkscape-extension-for-engraving-fonts/). Traversing several forks and renderings, this *Hershey Futural* might not be exactly the same as Dr. A.V. Hershey drew it but its principal idea remains: a one-line font composed of straight lines for complex typographic designs.

![Cathode ray tube display](images/cathode_ray_tube_display.png)

*Hershey Futural* has been cleaned up, some missing characters have been added and strokes of different thicknesses have been rendered. So far there are four different cuts from lighter to bolder: *-15*, *0*, *30*, *40* which are all located in the [outline/](outline) directory or the [hershey-futural-outline.zip](outline/hershey-futural-outline.zip).

The fonts by Dr. A.V. Hershey have ever since been available to the public domain. The *Hershey Futural* is licensed under the SIL Open Font License, thus free to be used.

## ToDo
- Apostrophe still in wrong direction; as usual.
